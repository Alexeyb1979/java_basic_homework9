package ua.com.danit.fs.dao;

import ua.com.danit.fs.entity.Family;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    protected List<Family> familiesList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return Collections.unmodifiableList(familiesList);
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index>=0 && index < familiesList.size()){
            return familiesList.get(index);
        } else {
            return null;
        }

    }

    @Override
    public Family saveFamily(Family entity) {
        if (familiesList.contains(entity)){
            int index = familiesList.indexOf(entity);
            familiesList.set(index, entity);
        } else {
            familiesList.add(entity);
        }
        return entity;
    }

    @Override
    public boolean deleteFamily(int index) {
        return false;
    }

    @Override
    public boolean deleteFamily(Family entity) {
        if (familiesList.contains(entity)){
            familiesList.remove(entity);
            return true;
        } else {
            return false;
        }
    }



}
