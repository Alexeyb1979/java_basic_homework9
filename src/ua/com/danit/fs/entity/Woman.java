package ua.com.danit.fs.entity;

import java.util.HashMap;

public final class Woman extends Human {

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, HashMap<String, String> schedule) {
        super(name, surname, year, schedule);
    }

    public Woman() {
    }

    public void makeup() {
        System.out.println(getName() + ", is doing make up!");
    }


}
