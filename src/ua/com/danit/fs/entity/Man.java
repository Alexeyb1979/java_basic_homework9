package ua.com.danit.fs.entity;

import java.util.HashMap;

public final class Man extends Human {
            public Man(String name, String surname, int year) {
            super(name, surname, year);
        }

        public Man(String name, String surname, int year, HashMap<String, String> schedule) {
            super(name, surname, year, schedule);
        }

        public Man() {
            }
       }
