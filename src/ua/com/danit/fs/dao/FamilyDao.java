package ua.com.danit.fs.dao;

import ua.com.danit.fs.entity.Family;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public interface FamilyDao {

    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    Family saveFamily (Family entity);
    boolean deleteFamily ( int index);
    boolean deleteFamily (Family entity);


}
