package ua.com.danit.fs.entity;

public enum Species {
    DOG,
    DOMESTIC_CAT,
    FISH,
    ROBO_CAT,
    UNKNOWN
}
